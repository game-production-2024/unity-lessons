using UnityEngine;


public class CodeLesson : MonoBehaviour
{
    //Dati

    [SerializeField] bool isHelloWorldText;
    [SerializeField] string textToCheck = "INSERT_TEXT_HERE";
    
    [Header("Tipi Primitivi")] //attributo//Tipi primitivi
    [Tooltip("Funziona solo se isGrounded � true")] 

    [SerializeField] bool canJump = true; //tipo valore
    public int numeroIntero = 3; //tipo valore
    public string testo = "Hello World"; //tipo riferimento

    [Header("Tipi custom")] //Tipi custom
    public Vector2 position = new Vector2(3f, 7.5f); //tipo valore
    public GameObject myGameObject; //tipo riferimento
    public Test test; //tipo riferimento


    ///////////////////////////////////////// 


    //Metodi che contengono gli algoritmi

    //chiamato alla generazione dell'oggetto
    void Awake()
    {
        
    }

    //eseguito quando l'oggetto viene distrutto
    void OnDestroy()
    {
        
    }

    //eseguito al primo frame di vita dell'oggetto
    void Start() //tra le parentesi l'input
    {
        
    }

    //eseguito ad ogni frame di vita dell'oggetto
    void Update()
    {
        isHelloWorldText = IsHelloWorldText(textToCheck);
    }

    //eseguito alla fine di ogni frame di vita dell'oggetto
    private void LateUpdate()
    {
        
    }

    //eseguito ogni tot frame di vita dell'oggetto per la fisica
    void FixedUpdate()
    {
        
    }

    bool IsHelloWorldText(string text)
    {
        if (text is "Hello World")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    int DammiLaSomma(int a, int b)
    {
        return a ^ b;
    }
}

