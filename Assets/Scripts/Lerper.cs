using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lerper : MonoBehaviour
{
    public Transform start;
    public Transform end;
    public Transform target;
    [Range(0, 1)] public float lerp = 0.5f;

    void Update()
    {
        target.position = Vector2.Lerp(start.position, end.position, lerp);
    }
}
