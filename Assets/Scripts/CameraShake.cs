using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public Transform camera;
    public float shakeDuration = 0.5f; // Durata dello shake
    public float shakeIntensity = 0.2f; // Intensità dello shake

    private Vector3 originalPos;
    private float shakeTimer = 0f;

    private void Start()
    {
        originalPos = camera.localPosition;
    }

    private void Update()
    {
        if (shakeTimer > 0)
        {
            // Genera una posizione casuale all'interno di uno spazio sferico e la moltiplica per l'intensità dello shake
            Vector2 shakePos = Random.insideUnitCircle * shakeIntensity;
            camera.localPosition = originalPos + new Vector3(shakePos.x, shakePos.y, 0);

            shakeTimer -= Time.deltaTime;
        }
        else
        {
            shakeTimer = 0f;
            camera.localPosition = originalPos;
        }
    }

    public void ShakeCamera()
    {
        shakeTimer = shakeDuration;
    }
}
