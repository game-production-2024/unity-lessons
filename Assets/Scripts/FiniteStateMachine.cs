using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiniteStateMachine : MonoBehaviour
{
    public BaseState currentState;
    List<BaseState> states = new List<BaseState>();

    void Awake()
    {
        states.AddRange(GetComponentsInChildren<BaseState>());

        foreach (BaseState state in states)
        {
            state.SetFiniteStateMachine(this);
        }
    }

    public void ChangeState(BaseState newState)
    {
        if (!states.Contains(newState))
        {
            return;
        }

        currentState.ExitState();
        currentState = newState;
        currentState.EnterState();
    }

    //overload
    public void ChangeState<T>() where T : BaseState
    {
        foreach (BaseState state in states) 
        {
            if (state.GetType() == typeof(T))
            {
                ChangeState(state);
                return;
            }
        }
    }

    void Update()
    {
        currentState.UpdateState();
    }

    void FixedUpdate()
    {
        currentState.FixedUpdateState();
    }
}
