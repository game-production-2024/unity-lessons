﻿using UnityEngine;

public abstract class BaseState : MonoBehaviour
{
    protected FiniteStateMachine fsm;

    public void SetFiniteStateMachine(FiniteStateMachine fsm)
    {
        this.fsm = fsm; 
    }

    public abstract void EnterState();
    public abstract void UpdateState();
    public abstract void ExitState();
    public abstract void FixedUpdateState();
}
