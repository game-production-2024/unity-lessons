using UnityEngine;
using UnityEngine.UI;

public class ReverseBar : MonoBehaviour
{
    public bool clickMe;
    public float timer = 1.7f;
    public Image bar;
    public Color startColor;
    public Color endColor;

    float elapsedTime = 0.0f;

    void Update()
    {
        if (clickMe)
        {
            ActivateReverseTimer();
            clickMe = false;
        }

        if (elapsedTime > 0.0f)
        {
            float t = Mathf.InverseLerp(0.0f, timer, elapsedTime);

            bar.fillAmount = t;
            bar.color = Color.Lerp(endColor, startColor, t);

            elapsedTime -= Time.deltaTime;

            if (elapsedTime <= 0.0f)
            {
                bar.fillAmount = 0.0f;
                bar.color = endColor;
            }
        }
    }

    public void ActivateReverseTimer()
    {
        elapsedTime = timer;
    }
}
