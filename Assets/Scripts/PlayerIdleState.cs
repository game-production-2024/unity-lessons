﻿using UnityEngine;

public class PlayerIdleState : BaseState
{
    public Rigidbody2D rb;

    public override void EnterState()
    {
        rb.velocity = Vector3.zero;
    }

    public override void UpdateState()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");

        if (horizontal != 0f)
        {
            fsm.ChangeState<PlayerWalkState>();
            return;
        }

        if (Input.GetMouseButtonDown(1))
        {
            fsm.ChangeState<PlayerProjectionState>();
        }
    }

    public override void FixedUpdateState()
    {

    }

    public override void ExitState()
    {
    }
}

