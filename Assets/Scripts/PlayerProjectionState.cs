﻿using UnityEngine;

public class PlayerProjectionState : BaseState
{
    public float projectionForce;
    public Rigidbody2D rb;
    public Animator animator;
    public Transform pivot;
    public LineRenderer lineRenderer;
    public GameObject anchor;

    public override void EnterState()
    {
        animator.SetBool("IsJumping", true);
        Project();
        UpdateLine();
        lineRenderer.gameObject.SetActive(true);
        anchor.SetActive(true);
    }

    public override void UpdateState()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Project();
        }

        UpdateLine();
    }

    public override void FixedUpdateState()
    {

    }

    public override void ExitState()
    {
        animator.SetBool("IsJumping", false);
        lineRenderer.gameObject.SetActive(false);
        anchor.SetActive(false);
    }

    void Project()
    {
        Vector2 mousePosition = Input.mousePosition;
        Vector2 mouseInWorld = Camera.main.ScreenToWorldPoint(mousePosition);
        Vector2 direction = (mouseInWorld - (Vector2)rb.transform.position).normalized;
        rb.AddForce(direction * projectionForce, ForceMode2D.Impulse);

        float x = 0.0f;

        if (mouseInWorld.x < rb.transform.position.x)
        {
            x = -1;
        }
        else
        {
            x = 1;
        }

        pivot.localScale = new Vector3(x, 1, 1);
        anchor.transform.position = mouseInWorld;
    }

    void UpdateLine()
    {
        lineRenderer.SetPosition(0, rb.transform.position);
        lineRenderer.SetPosition(1, anchor.transform.position);
    }
}

