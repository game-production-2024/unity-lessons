using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField] Transform target;

    // Metodo per teletrasportare un oggetto alla posizione target
    public void TeleportObject(Transform objectToTeleport)
    {
        if (target != null && objectToTeleport != null)
        {
            objectToTeleport.position = target.position;
        }
        else
        {
            Debug.LogWarning("Target o oggetto da teletrasportare non impostato!");
        }        
    }
}
