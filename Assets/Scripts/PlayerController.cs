using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementState
{
    None, ToLeft, ToRight
}

public class PlayerController : MonoBehaviour
{
    public MovementState currentState = MovementState.None;
    public Transform pivot;
    public float speed = 8.0f;
    public Rigidbody2D rb;
    public Animator animator;

    [Header("Debug")]
    public float horizontalAxis;


    void Update()
    {
        horizontalAxis = Input.GetAxisRaw("Horizontal");

        if (horizontalAxis == 0)
        {
            currentState = MovementState.None;
        }
        else if (horizontalAxis > 0)
        {
            currentState = MovementState.ToRight;
        }
        else if (horizontalAxis < 0)
        {
            currentState = MovementState.ToLeft;
        }

        switch (currentState)
        {
            case MovementState.None:
                animator.SetBool("IsWalking", false);
                break;

            case MovementState.ToLeft:
                animator.SetBool("IsWalking", true);
                pivot.localScale = new Vector3(-1, 1, 1);
                break;

            case MovementState.ToRight:
                animator.SetBool("IsWalking", true);
                pivot.localScale = new Vector3(1, 1, 1);
                break;
        }
    }

    void FixedUpdate()
    {
        float horizontalSpeed = horizontalAxis * speed;
        Vector2 velocity = new Vector2(horizontalSpeed, rb.velocity.y);
        rb.velocity = velocity;
    }

    //C:\PROJECTS\unity-lessons\Assets

}
