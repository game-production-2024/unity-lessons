﻿using UnityEngine;

public class PlayerWalkState : BaseState
{
    public float speed = 5f;
    public Rigidbody2D rb;
    public Animator animator;
    public Transform pivot;
    float horizontal;

    public override void EnterState()
    {
        animator.SetBool("IsWalking", true);
    }

    public override void UpdateState()
    {
        horizontal = Input.GetAxisRaw("Horizontal");

        if (horizontal == 0f)
        {
            fsm.ChangeState<PlayerIdleState>();
            return;
        }

        if (Input.GetMouseButtonDown(1))
        {
            fsm.ChangeState<PlayerProjectionState>();
            return;
        }

        pivot.transform.localScale = new Vector3(horizontal, 1f, 1f);
    }

    public override void FixedUpdateState()
    {
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
    }

    public override void ExitState()
    {
        animator.SetBool("IsWalking", false);
    }
}

