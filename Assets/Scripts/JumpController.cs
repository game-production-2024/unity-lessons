using UnityEngine;

public class JumpController : MonoBehaviour
{
    public KeyCode jumpKey = KeyCode.Space; // Il tasto di salto
    public float jumpForce = 10f; // La forza di salto

    private Rigidbody2D rb;
    private bool canJump = true;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (canJump && Input.GetKeyDown(jumpKey))
        {
            Jump();
        }
    }

    public void SetCanJump(bool value)
    {
        canJump = value;
    }

    void Jump()
    {
        if (rb != null)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0f); // Resetta la velocit� verticale prima di saltare
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse); // Applica la forza di salto
        }
        else
        {
            Debug.LogWarning("Rigidbody2D non trovato. Assicurati che il GameObject abbia un Rigidbody2D.");
        }
    }
}
